typedef enum messageDef {
Emergency_Stop = 1, /* Length: 0, Data: NULL */
Request_Pickup_R1 = 2, /* Length: 0, Data: NULL */
Request_Drop_R1 = 3, /* Length: 0, Data: NULL */
Piece_At_Conveyor = 4, /* Length: 0, Data: NULL */
Request_Pickup_R2 = 5, /* Length: 0, Data: NULL */
Error = 6, /* Length: 0, Data: NULL */
Pause_System = 7, /* Length: 0, Data: NULL */
Resume = 8, /* Length: 0, Data: NULL */
System_Run = 9 /* Length: 0, Data: NUL */
} messageDef;